import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonComponent } from './app/register/person/person.component';
import { HomePageComponent } from './app/register/home-page/home-page.component';
import { EnderecoComponent } from './app/register/endereco/endereco.component';
import { PersonListComponent } from './app/register/person/person-list/person-list.component';
import { EnderecoListComponent } from './app/register/endereco/endereco-list/endereco-list.component';


const appRoutes: Routes = [
    { path: '', redirectTo: 'pessoaList', pathMatch: 'full' },
    { path: 'pessoa/:id', pathMatch: 'full', component: PersonComponent },
    { path: 'pessoa', pathMatch: 'full', component: PersonComponent },
    { path: 'endereco', pathMatch: 'full', component: EnderecoComponent },
    { path: 'pessoaList', pathMatch: 'full', component: PersonListComponent },
    { path: 'enderecoList', pathMatch: 'full', component: EnderecoListComponent },
    { path: 'endereco/:id', pathMatch: 'full', component: EnderecoComponent },
    { path: '**', redirectTo: 'basic-information' },
    { path: 'home', pathMatch: 'full', component: HomePageComponent },

];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: []
  })
  
  export class AppRoutingModule { }