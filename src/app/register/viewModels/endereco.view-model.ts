export class EnderecoViewModel{
    id : number;
    rua: string;
    numero: number;
    cep : string;
    bairro: string;
    

    constructor(e : any){
        e.id = e.id != null ? e.id : null;
        e.rua = e.rua != null ? e.rua : null;
        e.numero = e.numero != null ? e.numero : null;
        e.bairro = e.bairro != null ? e.bairro : null;
    }
}