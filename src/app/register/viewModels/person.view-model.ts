import { EnderecoViewModel } from "./endereco.view-model";

export class PersonViewModel{

    id: number;
    nome: string;
    dataNascimento: Date;
    cpf: string;
    sexo_id: number;   
    // endereco_id?: number; 
    // endereco : EnderecoViewModel;

    constructor(p: any){
    
        this.id = p.id != null ? p.id : null;
        this.nome = p.nome != null ? p.nome : null;
        this.dataNascimento = p.dataNascimento ? p.dataNascimento : null;
        this.cpf = p.cpf ? p.cpf : null;
        this.sexo_id = p.sexo_id ? p.sexo_id : null;
    }
}

