import { Component, OnInit } from '@angular/core';
import { PersonService } from './person.service';
import { PersonViewModel } from '../viewModels/person.view-model';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  providers: [PersonService]
})
export class PersonComponent implements OnInit {

  personData: PersonViewModel = new PersonViewModel({});
  executePost: boolean = true;
  personForm: FormGroup;
  person_id: number;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private personService: PersonService,
    private rota: ActivatedRoute

  ) { this.person_id = rota.snapshot.params['id']; }

  ngOnInit() {
    this.buildForm();
    this.getPersonByPersonId();
  }

  buildForm() {
    this.personForm = this.formBuilder.group({
      'nome': ['', Validators.required],
      'cpf': ['', Validators.required],
      'dataNascimento': ['', Validators.required],
      'sexo': ['', Validators.required]
    });
  }


  getPersonByPersonId() {

    const idPerson = this.person_id;
    this.personService.getPersonById(idPerson).subscribe(
      (data: any) => {
        if (data !== undefined) {
          if (data.id !== null) {
            this.personData = new PersonViewModel(data);
            this.personData.id = parseInt(data.id, 10);
            this.personData.dataNascimento = moment(this.personData.dataNascimento, 'YYYY-MM-DD').toDate();

          }
        }
      },
      err => {
        const errorSave = err;
        if (errorSave.errors != null) {
          if (errorSave.errors[0].Status === 404) {
            this.executePost = true;
          }
        }
      });
  }


  saveData() {
    if (this.personData.id != null) {
      this.executePost = false;
    }

    if (this.executePost) {
      this.personService.postPerson(this.personData).subscribe(result => {
        if (result) {
          const dataReturn = result.Data;
          this.router.navigate(['endereco']);
        }
      });
    }

    else {
      if (this.executePost === false) {
        if (this.personData.id != null) {
          // this.parseDataSend();
          console.log(this.personData);
          this.personService.updatePerson(this.personData).subscribe(res => {
  
          });
        }
      }
    }
  }

  submitUpdate() {
    this.personService.updatePerson(this.personData).subscribe(result => {
      alert("Atualizado com sucesso");
    }, error => {
      var data = JSON.parse(error._body);
    });
  }

  validContinue() {
    if (this.personForm.valid) {
      this.saveData();
    }
  }

  parseDataSend() {

  }

  disableNext() {

  }
}
