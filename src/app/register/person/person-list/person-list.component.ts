import { Component, OnInit } from '@angular/core';
import { PersonService } from '../person.service';
import { Http } from '@angular/http';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  providers: [PersonService]
})

export class PersonListComponent implements OnInit {

  mensagem: string = "";
  personList: any[];

  constructor(http: Http, private personService: PersonService) { }

  ngOnInit() {
    this.personService.getPerson().subscribe(result => {
      this.personList = result;
    });
  }


  excluirPerson(person: any) {    
    this.personService.deletePerson(person.id).subscribe(() => {
      let indicePerson = this.personList.indexOf(person);

      if (indicePerson > -1) {
        this.mensagem = 'Pessoa removido com sucesso.';
        this.personList.slice(indicePerson, 1);
        this.atualizarLista();
      }
    });
  }

  atualizarLista() {

    this.personService.getPerson().subscribe(result => {
      this.personList = result;
    });
  }


}