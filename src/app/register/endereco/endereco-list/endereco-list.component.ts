import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { EnderecoService } from '../endereco.service';

@Component({
  selector: 'app-endereco-list',
  templateUrl: './endereco-list.component.html',
  providers: [EnderecoService]  
})
export class EnderecoListComponent implements OnInit {

  mensagem: string = "";
  enderecoList: any[];

  constructor(http: Http, private enderecoService: EnderecoService) { }

  ngOnInit() {
    this.enderecoService.getEndereco().subscribe(result => {
      this.enderecoList = result;
    });
  }


  excluirPerson(endereco: any) {
    this.enderecoService.deleteEndereco(endereco.id).subscribe(() => {
      let indicePerson = this.enderecoList.indexOf(endereco);

      if (indicePerson > -1) {
        this.mensagem = 'Endereço removido com sucesso.';
        this.enderecoList.slice(indicePerson, 1);
        this.atualizarLista();
      }
    });
  }

  atualizarLista() {

    this.enderecoService.getEndereco().subscribe(result => {
      this.enderecoList = result;
    });
  }


}
