import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EnderecoViewModel } from '../viewModels/endereco.view-model';
import { EnderecoService } from './endereco.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { KeysCodeEnum } from '../../shared/enums/key-binds.enum';



@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  providers: [EnderecoService]
})
export class EnderecoComponent implements OnInit {

  enderecoData: EnderecoViewModel = new EnderecoViewModel({});
  endereco_id: number;
  selectPostalCode: any;
  executePost: boolean = true;
  showFullAddress: boolean;
  enderecoForm: FormGroup;
  city: any;
  uf: any;

  @ViewChild('inputNumber') inputNumber: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private enderecoService: EnderecoService,

    private rota: ActivatedRoute
  ) { this.endereco_id = rota.snapshot.params['id']; }

  ngOnInit() {
    this.buildForm()
    this.getEnderecoById();
  }

  buildForm() {
    this.enderecoForm = this.formBuilder.group({
      'cep': ['', Validators.required],
      'cidade': ['', Validators.required],
      'estado': ['', Validators.required],
      'rua': ['', Validators.required],
      'bairro': ['', Validators.required],
      'numero': ['', Validators.required],
    });
  }


  getEnderecoById() {

    const idEndereco = this.endereco_id;
    this.enderecoService.getEnderecoById(idEndereco).subscribe(
      (data: any) => {
        if (data !== undefined) {
          if (data.id !== null) {
            this.enderecoData = new EnderecoViewModel(data);
            this.enderecoData.id = parseInt(data.id, 10);
            this.enderecoData.cep = data.cep;
          }
        }
      },
      err => {
        const errorSave = err;
        if (errorSave.errors != null) {
          if (errorSave.errors[0].Status === 404) {
            this.executePost = true;
          }
        }
      });
  }


  searchCep(event?: any) {    
    if (this.enderecoData.cep && this.enderecoData.cep.length === 9) {
      const zipCode = this.enderecoData.cep;
      this.enderecoService.getAddressByPostalCode(zipCode).subscribe(res => {
        if (res.erro) {
          // this.validCep = false;
          // this.resetNotCep();
          // this.addressForm.controls['city'].disable();
        } else {
          // this.validCep = true;
          this.parseCepModel(res);
          this.showFullAddress = true;
          if (this.inputNumber
            && event.keyCode !== KeysCodeEnum.TAB
            && event.keyCode !== KeysCodeEnum.SHIFT
            && event.keyCode !== KeysCodeEnum.CTRLA
            && event.keyCode !== KeysCodeEnum.LEFT_ARROW
            && event.keyCode !== KeysCodeEnum.CTRL
            && event.keyCode !== KeysCodeEnum.RIGHT_ARROW) {
            this.inputNumber.nativeElement.focus();
          }
        }
      });
    }
  }


  parseCepModel(data: any) {
    this.enderecoData.cep = data.cep;
    this.enderecoData.rua = data.logradouro;
    this.enderecoData.numero = data.numero;
    this.enderecoData.bairro = data.bairro;
    this.uf = data.uf;
    this.city = data.localidade;

  }

  validContinue() {
    if (this.enderecoForm.valid) {
      this.saveData();
    }
  }

  saveData() {
    if (this.enderecoData.id != null) {
      this.executePost = false;
    }

    if (this.executePost) {
      this.enderecoService.postEndereco(this.enderecoData).subscribe(result => {
        if (result) {
          const dataReturn = result.Data;
          this.router.navigate(['enderecoList']);
        }
      });
    }

    else {
      if (this.executePost === false) {
        if (this.enderecoData.id != null) {

          this.enderecoService.updateEndereco(this.enderecoData).subscribe(res => {
            this.executePost = false;
          });
        }
      }
    }
  }

  submitUpdate() {
    this.enderecoService.updateEndereco(this.enderecoData).subscribe(result => {
      alert("Atualizado com sucesso");
    }, error => {
      var data = JSON.parse(error._body);
    });
  }

}
