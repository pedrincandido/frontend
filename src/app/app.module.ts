import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DateValueAccessorModule } from 'angular-date-value-accessor';
import {Ng2MaskModule} from 'ng2-mask'

import { AppComponent } from './app.component';
import { PersonComponent } from './register/person/person.component';
import { HomePageComponent } from './register/home-page/home-page.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HeadbarComponent } from './shared/headbar/headbar.component';
import { SubMenuComponent } from './shared/sub-menu/sub-menu.component';
import { AppRoutingModule } from '../routing';
import { EnderecoComponent } from './register/endereco/endereco.component';

import { PersonListComponent } from './register/person/person-list/person-list.component';
import { EnderecoListComponent } from './register/endereco/endereco-list/endereco-list.component';


@NgModule({
  declarations: [
    AppComponent,
    PersonComponent,
    HomePageComponent,
    FooterComponent,
    HeadbarComponent,
    SubMenuComponent,
    EnderecoComponent,
    PersonListComponent,
    EnderecoListComponent
    
  ],
  imports: [
    Ng2MaskModule,
    RouterModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
