
/* Create URL Default APIs */
const URL_DEFAULT_API = 'localhost';
const PROTOCOL_HTTPS = false;
// const PORT_DEFAULT_API = PROTOCOL_HTTPS ? '8243' : '8280';
const HTTP_API = PROTOCOL_HTTPS ? 'https://' : 'http://';
const URL_API = HTTP_API + URL_DEFAULT_API;
//+ ':'  + PORT_DEFAULT_API + '/';

/* Component routes */
enum ROUTER_OPTIONS {
  'basic-information',
  'personal-information',
  'identification-document',
  'address',
  'professional-information',
  'professional-finance-information',
  'bank-details',
  'issuer-of-order',  
  'investor-profile',
  'declaration',
  'terms',
  'finish-register'
}
/* Control name APIs */
const PERSON_API = ':1080';


/*Services endpoints control names*/
const PERSON_ENDPOINT = '/pessoa';
const ENDERECO_ENDPOINT = '/endereco';
/* ViaCep URL API  */
const VIA_CEP_API = 'https://viacep.com.br/ws/';

export const environment = {  
  production: false,
  envName: 'dev',
  version: '0.0.1',
  ROUTER_OPTIONS: ROUTER_OPTIONS,

  ADDRESS_VIA_CEP_API: VIA_CEP_API,
  PERSON_API: URL_API + PERSON_API,
  ENDERECO_API: URL_API + PERSON_API,
  
  PERSON_ENDPOINT: PERSON_ENDPOINT,
  ENDERECO_ENDPOINT: ENDERECO_ENDPOINT
};


